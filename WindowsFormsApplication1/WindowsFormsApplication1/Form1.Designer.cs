﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Juoda = new System.Windows.Forms.Button();
            this.Laukelis = new System.Windows.Forms.TextBox();
            this.Late = new System.Windows.Forms.Button();
            this.Balta = new System.Windows.Forms.Button();
            this.Kapuchino = new System.Windows.Forms.Button();
            this.vienas = new System.Windows.Forms.Button();
            this.Laukelis2 = new System.Windows.Forms.TextBox();
            this.Du = new System.Windows.Forms.Button();
            this.Darymobut = new System.Windows.Forms.Button();
            this.Valyti = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.Išvalytikava = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Juoda
            // 
            this.Juoda.Location = new System.Drawing.Point(12, 57);
            this.Juoda.Name = "Juoda";
            this.Juoda.Size = new System.Drawing.Size(75, 23);
            this.Juoda.TabIndex = 0;
            this.Juoda.Text = "Juoda ";
            this.Juoda.UseVisualStyleBackColor = true;
            this.Juoda.Click += new System.EventHandler(this.button1_Click);
            // 
            // Laukelis
            // 
            this.Laukelis.Location = new System.Drawing.Point(12, 12);
            this.Laukelis.Name = "Laukelis";
            this.Laukelis.Size = new System.Drawing.Size(75, 20);
            this.Laukelis.TabIndex = 1;
            this.Laukelis.TextChanged += new System.EventHandler(this.Laukelis_TextChanged);
            // 
            // Late
            // 
            this.Late.Location = new System.Drawing.Point(12, 86);
            this.Late.Name = "Late";
            this.Late.Size = new System.Drawing.Size(75, 23);
            this.Late.TabIndex = 2;
            this.Late.Text = "Late";
            this.Late.UseVisualStyleBackColor = true;
            this.Late.Click += new System.EventHandler(this.Late_Click);
            // 
            // Balta
            // 
            this.Balta.Location = new System.Drawing.Point(12, 115);
            this.Balta.Name = "Balta";
            this.Balta.Size = new System.Drawing.Size(75, 23);
            this.Balta.TabIndex = 3;
            this.Balta.Text = "Balta";
            this.Balta.UseVisualStyleBackColor = true;
            this.Balta.Click += new System.EventHandler(this.Balta_Click);
            // 
            // Kapuchino
            // 
            this.Kapuchino.Location = new System.Drawing.Point(12, 144);
            this.Kapuchino.Name = "Kapuchino";
            this.Kapuchino.Size = new System.Drawing.Size(75, 23);
            this.Kapuchino.TabIndex = 4;
            this.Kapuchino.Text = "Kapuchino";
            this.Kapuchino.UseVisualStyleBackColor = true;
            this.Kapuchino.Click += new System.EventHandler(this.Kapuchino_Click);
            // 
            // vienas
            // 
            this.vienas.Location = new System.Drawing.Point(102, 57);
            this.vienas.Name = "vienas";
            this.vienas.Size = new System.Drawing.Size(55, 23);
            this.vienas.TabIndex = 5;
            this.vienas.Text = "Vienas Šaukštelis";
            this.vienas.UseVisualStyleBackColor = true;
            this.vienas.Click += new System.EventHandler(this.vienas_Click);
            // 
            // Laukelis2
            // 
            this.Laukelis2.Location = new System.Drawing.Point(102, 12);
            this.Laukelis2.Name = "Laukelis2";
            this.Laukelis2.Size = new System.Drawing.Size(75, 20);
            this.Laukelis2.TabIndex = 6;
            this.Laukelis2.TextChanged += new System.EventHandler(this.Laukelis2_TextChanged);
            // 
            // Du
            // 
            this.Du.Location = new System.Drawing.Point(102, 86);
            this.Du.Name = "Du";
            this.Du.Size = new System.Drawing.Size(55, 23);
            this.Du.TabIndex = 7;
            this.Du.Text = "Du šaukteliai";
            this.Du.UseVisualStyleBackColor = true;
            this.Du.Click += new System.EventHandler(this.Du_Click);
            // 
            // Darymobut
            // 
            this.Darymobut.Location = new System.Drawing.Point(197, 57);
            this.Darymobut.Name = "Darymobut";
            this.Darymobut.Size = new System.Drawing.Size(211, 23);
            this.Darymobut.TabIndex = 8;
            this.Darymobut.Text = "Daryti kavą ";
            this.Darymobut.UseVisualStyleBackColor = true;
            this.Darymobut.Click += new System.EventHandler(this.Darymobut_Click);
            // 
            // Valyti
            // 
            this.Valyti.Location = new System.Drawing.Point(197, 86);
            this.Valyti.Name = "Valyti";
            this.Valyti.Size = new System.Drawing.Size(211, 23);
            this.Valyti.TabIndex = 9;
            this.Valyti.Text = "Išvalyti ";
            this.Valyti.UseVisualStyleBackColor = true;
            this.Valyti.Click += new System.EventHandler(this.Valyti_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(102, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Cukraus kiekis";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Kavos ";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(197, 12);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(211, 20);
            this.textBox3.TabIndex = 12;
            this.textBox3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // Išvalytikava
            // 
            this.Išvalytikava.Location = new System.Drawing.Point(12, 173);
            this.Išvalytikava.Name = "Išvalytikava";
            this.Išvalytikava.Size = new System.Drawing.Size(75, 23);
            this.Išvalytikava.TabIndex = 13;
            this.Išvalytikava.Text = "Išvalyti ";
            this.Išvalytikava.UseVisualStyleBackColor = true;
            this.Išvalytikava.Click += new System.EventHandler(this.Išvalytikava_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(105, 115);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(52, 23);
            this.button2.TabIndex = 14;
            this.button2.Text = "Išvalyti ";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(420, 261);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.Išvalytikava);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Valyti);
            this.Controls.Add(this.Darymobut);
            this.Controls.Add(this.Du);
            this.Controls.Add(this.Laukelis2);
            this.Controls.Add(this.vienas);
            this.Controls.Add(this.Kapuchino);
            this.Controls.Add(this.Balta);
            this.Controls.Add(this.Late);
            this.Controls.Add(this.Laukelis);
            this.Controls.Add(this.Juoda);
            this.Name = "Form1";
            this.Text = "Kavos aparatas ";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Juoda;
        private System.Windows.Forms.TextBox Laukelis;
        private System.Windows.Forms.Button Late;
        private System.Windows.Forms.Button Balta;
        private System.Windows.Forms.Button Kapuchino;
        private System.Windows.Forms.Button vienas;
        private System.Windows.Forms.TextBox Laukelis2;
        private System.Windows.Forms.Button Du;
        private System.Windows.Forms.Button Darymobut;
        private System.Windows.Forms.Button Valyti;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button Išvalytikava;
        private System.Windows.Forms.Button button2;
    }
}

